using System;

namespace Corazon.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static long ToEpochMilliseconds(this DateTime date)
            => ((DateTimeOffset)date).ToUnixTimeMilliseconds();
    }
}