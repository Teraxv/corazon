using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Corazon.Core.Entities 
{
    public class Notification 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int Type { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public User DeclarantUser { get; set; }
        public User AssignedUser { get; set; }
        public int Status { get; set; }
        public DateTime SaveTime { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}