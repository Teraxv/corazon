using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Corazon.Core.Entities
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }
        public int AccessLevel { get; set; }
        public int Deleted { get; set; }
        public DateTime DateCreated { get; set; }

        public ICollection<Notification> DeclaredNotifications { get; set; }
        public ICollection<Notification> AssignedNotifications { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}