using System;
using Microsoft.EntityFrameworkCore;

namespace Corazon.Core.Entities.Contexts {
    public class EntityContext : DbContext {
        public EntityContext (DbContextOptions options) : base (options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            modelBuilder.Entity<User> ()
                .HasKey (o => o.Id);
            modelBuilder.Entity<User> ()
                .Property (o => o.Id).ValueGeneratedOnAdd ();
            modelBuilder.Entity<Notification> ()
                .HasKey (o => o.Id);
            modelBuilder.Entity<Notification> ()
                .Property (o => o.Id).ValueGeneratedOnAdd ();
            modelBuilder.Entity<Comment> ()
                .HasKey (o => o.Id);
            modelBuilder.Entity<Comment> ()
                .Property (o => o.Id).ValueGeneratedOnAdd ();

            modelBuilder.Entity<User> ()
                .HasMany (o => o.DeclaredNotifications)
                .WithOne (o => o.DeclarantUser)
                .OnDelete (DeleteBehavior.SetNull);

            modelBuilder.Entity<User> ()
                .HasMany (o => o.AssignedNotifications)
                .WithOne (o => o.AssignedUser)
                .OnDelete (DeleteBehavior.SetNull);

            modelBuilder.Entity<User> ()
                .HasData (
                    new User {
                        Name = "serwis",
                            Id = 1, 
                            AccessLevel = 3,
                            Email = "serwis",
                            Password = "zaq12wsx"
                    }
                );

            modelBuilder.Entity<User> ()
                .HasMany (o => o.Comments)
                .WithOne (o => o.User)
                .OnDelete (DeleteBehavior.Cascade);

            modelBuilder.Entity<Notification> ()
                .HasMany (o => o.Comments)
                .WithOne (o => o.Notification)
                .OnDelete (DeleteBehavior.Cascade);
        }
    }
}