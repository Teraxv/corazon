using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Corazon.Core.Entities
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public Notification Notification { get; set; }
        [Required]
        public User User { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime SaveTime { get; set; }
    }
}