namespace Corazon.Core.Domain
{
    public class JsonWebToken
    {
        public long UserId { get; set; }
        public string AccessToken { get; set; }
        public int AccessLevel { get; set; }
        public long ExpiresTicks { get; set; }
    }
}