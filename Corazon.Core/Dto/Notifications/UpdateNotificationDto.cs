using System.ComponentModel.DataAnnotations;

namespace Corazon.Core.Dto.Notifications
{
    public class UpdateNotificationDto 
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [Range (0, 1)]
        public int Type { get; set; }

        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public int AssignedUserId { get; set; }

        [Required]
        [Range (0, 4)]
        public int Status { get; set; }
    }
}