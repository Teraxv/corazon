using System.ComponentModel.DataAnnotations;

namespace Corazon.Core.Dto.Notifications
{
    public class NewNotificationDto 
    {
        [Required]
        [Range (0, 1)]
        public int Type { get; set; }

        [Required]
        public string Title { get; set; }
        public string Description { get; set; }

        [Required]
        [Range (1, int.MaxValue)]
        public int DeclarantUserId { get; set; }
    }
}