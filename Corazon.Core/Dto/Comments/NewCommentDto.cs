using System.ComponentModel.DataAnnotations;

namespace Corazon.Core.Dto.Comments
{
    public class NewCommentDto
    {
        [Required]
        [Range(1,int.MaxValue)]
        public int UserId { get; set; }
        [Required]
        [Range(1,int.MaxValue)]
        public int NotificationId { get; set; }
        [Required]
        public string Description { get; set; }
    }
}