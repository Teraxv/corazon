using System;
namespace Corazon.Core.Dto.Comments
{
    public class CommentDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
        public DateTime SaveTime { get; set; }
    }
}