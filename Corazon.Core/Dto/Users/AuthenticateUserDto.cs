using System.ComponentModel.DataAnnotations;

namespace Corazon.Core.Dto.Users
{
    public class AuthenticateUserDto 
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}