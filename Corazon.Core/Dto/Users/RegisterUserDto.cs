using System.ComponentModel.DataAnnotations;

namespace Corazon.Core.Dto.Users
{
    public class RegisterUserDto 
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Password { get; set; }
    }
}