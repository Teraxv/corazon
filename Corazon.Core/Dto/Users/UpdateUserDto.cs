using System.ComponentModel.DataAnnotations;

namespace Corazon.Core.Dto.Users 
{
    public class UpdateUserDto 
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [Range (0, 2)]
        public int AccessLevel { get; set; }
    }
}