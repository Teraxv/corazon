using System.Threading.Tasks;
using Corazon.Core.Dto.Comments;
using Microsoft.AspNetCore.SignalR;

namespace Corazon.Infrastructure.Hubs {
    public class NotificationHub : Hub<INotificationHub> {
        public async Task AddToGroupAsync (string group) {
            await Groups.AddToGroupAsync (Context.ConnectionId, group);
        }

        public async Task RemoveFromGroupAsync (string group) {
            await Groups.RemoveFromGroupAsync (Context.ConnectionId, group);
        }

        public async Task CreateCommentAsync (CommentDto comment, string group) {
            await Clients.Group (group).CreateCommentAsync (comment);
        }

        public async Task DeleteCommentAsync (int id, string group) {
            await Clients.Group (group).DeleteCommentAsync (id);
        }

        public async Task ChangeNotificationStatusAsync (int notificationId, int status) {
            await Clients.All.ChangeNotificationStatusAsync (notificationId, status);
        }
    }
}