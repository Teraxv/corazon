using System.Threading.Tasks;
using Corazon.Core.Dto.Comments;

namespace Corazon.Infrastructure.Hubs {
    public interface INotificationHub {

        Task CreateCommentAsync (CommentDto comment);
        Task DeleteCommentAsync (int commentId);
        Task ChangeNotificationStatusAsync (int notificationId, int status);
    }
}