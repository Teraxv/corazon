using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Corazon.Core.Domain;
using Corazon.Core.Dto.Users;
using Corazon.Core.Entities;
using Corazon.Core.Entities.Contexts;
using Corazon.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Corazon.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IAuthorizationService authorizationService;
        private readonly EntityContext context;
        private readonly IMapper mapper;

        public UserService(IAuthorizationService authorizationService, EntityContext context, IMapper mapper)
        {
            this.authorizationService = authorizationService;
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<JsonWebToken> AuthenticateAsync(AuthenticateUserDto model)
        {
            var user = await context.Users.AsNoTracking()
                .FirstOrDefaultAsync(e => e.Email == model.Email && e.Password == model.Password && e.Deleted == 0);
            if (user == null)
                throw new ArgumentException("Invalid Credentials");
            return authorizationService.GenerateToken(user.Id, user.AccessLevel);
        }

        public async Task Register(RegisterUserDto model)
        {
            var entity = mapper.Map<User>(model);
            entity.DateCreated = DateTime.UtcNow;
            await context.Users.AddAsync(entity);
            await context.SaveChangesAsync();
        }

        public IQueryable<User> Browse()
            => context.Users.AsNoTracking().Include(o => o.AssignedNotifications).Include(o => o.DeclaredNotifications);

        public async Task<User> GetAsync(int id)
            => await Browse().FirstOrDefaultAsync(e => e.Id == id);

        public async Task UpdateAsync(UpdateUserDto model)
        {
            var entity = await GetAsync(model.Id);
            entity.Email = model.Email;
            entity.Name = model.Name;
            entity.AccessLevel = model.AccessLevel;
            context.Users.Update(entity);
            await context.SaveChangesAsync();
        }

        public async Task UpdatePasswordAsync(int id, string password)
        {
            var entity = await GetAsync(id);
            entity.Password = password;
            context.Users.Update(entity);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await GetAsync(id);
            entity.Deleted = 1;
            context.Users.Update(entity);
            await context.SaveChangesAsync();
        }

        public async Task RestoreAsync(int id)
        {
            var entity = await GetAsync(id);
            entity.Deleted = 0;
            context.Users.Update(entity);
            await context.SaveChangesAsync();
        }
    }
}