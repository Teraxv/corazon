using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Corazon.Core.Domain;
using Corazon.Core.Extensions;
using Corazon.Infrastructure.Options;
using Corazon.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Corazon.Infrastructure.Services
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly JwtSecurityTokenHandler tokenHandler;
        private readonly SigningCredentials signingCredentials;
        private readonly ILogger<AuthorizationService> logger;
        private readonly JwtOptions jwtOptions;

        public AuthorizationService(JwtOptions jwtOptions, ILogger<AuthorizationService> logger)
        {
            this.jwtOptions = jwtOptions;
            tokenHandler = new JwtSecurityTokenHandler();
            signingCredentials = new SigningCredentials(jwtOptions.GetSecurityKey(), SecurityAlgorithms.HmacSha256);
            this.logger = logger;
        }

        public JsonWebToken GenerateToken(int userId, int accessLevel)
        {
            logger.LogTrace($"Preparing access token for User with Id '{userId}'");
            var now = DateTime.Now;
            var expires = now.AddMinutes(jwtOptions.ExpiryMinutes);

            var ret = new JsonWebToken
            {
                UserId = userId,
                AccessToken = CreateJwtToken(userId, accessLevel, now, expires),
                AccessLevel = accessLevel,
                ExpiresTicks = expires.ToEpochMilliseconds()
            };

            logger.LogDebug($"Prepared access token for User with Id '{userId}'");
            return ret;
        }

        private string CreateJwtToken(int userId, int accessLevel, DateTime issued, DateTime expires)
        {
            var issuedEpoch = issued.ToEpochMilliseconds().ToString();
            var uniqueName = userId.ToString();

            var tokenId = Guid.NewGuid();
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, uniqueName),
                new Claim(JwtRegisteredClaimNames.UniqueName, uniqueName),
                new Claim(JwtRegisteredClaimNames.Jti, tokenId.ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, issuedEpoch),
                new Claim(jwtOptions.AccessClaimName, accessLevel.ToString())
            };

            return tokenHandler.WriteToken(new JwtSecurityToken(
                issuer: jwtOptions.Issuer,
                claims: claims,
                notBefore: issued,
                expires: expires,
                signingCredentials: signingCredentials
            ));
        }
    }
}