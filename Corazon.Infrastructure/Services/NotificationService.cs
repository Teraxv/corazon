using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Corazon.Core.Dto.Notifications;
using Corazon.Core.Entities;
using Corazon.Core.Entities.Contexts;
using Corazon.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Corazon.Infrastructure.Services
{
    public class NotificationService : INotificationService
    {
        private readonly EntityContext context;
        private readonly IMapper mapper;

        public NotificationService(EntityContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public IQueryable<Notification> Browse()
            => context.Notifications.AsNoTracking().Include(o => o.AssignedUser).Include(o => o.DeclarantUser);

        public IQueryable<Notification> BrowseByAssignedUser(int userId)
            => Browse().Where(o => o.AssignedUser.Id == userId).Include(o => o.AssignedUser).Include(o => o.DeclarantUser);

        public IQueryable<Notification> BrowseByDeclarantUser(int userId)
            => Browse().Where(o => o.DeclarantUser.Id == userId).Include(o => o.AssignedUser).Include(o => o.DeclarantUser);

        public async Task Create(NewNotificationDto model)
        {
            var entity = mapper.Map<Notification>(model);
            var declarent = await context.Users.FirstOrDefaultAsync(o => o.Id == model.DeclarantUserId);
            entity.DeclarantUser = declarent;
            await context.Notifications.AddAsync(entity);
            await context.SaveChangesAsync();
        }

        public async Task UpdateAsync(UpdateNotificationDto model)
        {
            var entity = await GetNotification(model.Id);
            var assigned = await context.Users.FirstOrDefaultAsync(o => o.Id == model.AssignedUserId);
            entity.AssignedUser = assigned;
            entity.Type = model.Type;
            entity.Title = model.Title;
            entity.Description = model.Description;
            entity.Status = model.Status;
            context.Notifications.Update(entity);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var notification = await GetNotification(id);
            context.Notifications.Remove(notification);
            await context.SaveChangesAsync();
        }

        private async Task<Notification> GetNotification(int id)
            => await context.Notifications.AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
    }
}