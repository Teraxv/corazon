using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Corazon.Core.Dto.Comments;
using Corazon.Core.Entities;

namespace Corazon.Infrastructure.Services.Interfaces
{
    public interface ICommentService
    {
        Task<IEnumerable<CommentDto>> Get(int notificationId, int offset, int count);
        Task Create(NewCommentDto commentDto);
        Task Delete(int id);
    }
}