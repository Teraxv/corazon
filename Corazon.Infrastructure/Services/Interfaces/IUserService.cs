using System.Linq;
using System.Threading.Tasks;
using Corazon.Core.Domain;
using Corazon.Core.Dto.Users;
using Corazon.Core.Entities;

namespace Corazon.Infrastructure.Services.Interfaces
{
    public interface IUserService
    {
        Task<JsonWebToken> AuthenticateAsync(AuthenticateUserDto model);
        Task Register(RegisterUserDto model);
        IQueryable<User> Browse();
        Task<User> GetAsync(int id);
        Task UpdateAsync(UpdateUserDto model);
        Task UpdatePasswordAsync(int id, string password);
        Task DeleteAsync(int id);
        Task RestoreAsync(int id);
    }
}