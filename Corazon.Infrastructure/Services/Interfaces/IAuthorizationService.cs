using Corazon.Core.Domain;

namespace Corazon.Infrastructure.Services.Interfaces
{
    public interface IAuthorizationService
    {
        JsonWebToken GenerateToken(int userId, int accessLevel);
    }
}