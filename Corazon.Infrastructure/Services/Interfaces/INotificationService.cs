using System.Linq;
using System.Threading.Tasks;
using Corazon.Core.Dto.Notifications;
using Corazon.Core.Entities;

namespace Corazon.Infrastructure.Services.Interfaces
{
    public interface INotificationService
    {
        IQueryable<Notification> Browse();
        IQueryable<Notification> BrowseByAssignedUser(int userId);
        IQueryable<Notification> BrowseByDeclarantUser(int userId);
        Task Create(NewNotificationDto model);
        Task UpdateAsync(UpdateNotificationDto model);
        Task DeleteAsync(int id);
    }
}