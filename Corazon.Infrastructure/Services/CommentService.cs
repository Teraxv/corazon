using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Corazon.Core.Dto.Comments;
using Corazon.Core.Entities;
using Corazon.Core.Entities.Contexts;
using Corazon.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Corazon.Infrastructure.Services {
    public class CommentService : ICommentService {
        private readonly EntityContext context;
        private readonly IMapper mapper;

        public CommentService (EntityContext context, IMapper mapper) {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<CommentDto>> Get (int notificationId, int offset, int count) {
            var comments = context.Comments
                .AsNoTracking ()
                .Include (o => o.Notification)
                .Include (o => o.User)
                .Where (o => o.Notification.Id == notificationId)
                .OrderByDescending (o => o.Description)
                .Skip (offset)
                .Take (count);

            return mapper.Map<IEnumerable<CommentDto>> (await comments.ToListAsync ());
        }

        public async Task Create (NewCommentDto commentDto) {
            var entity = mapper.Map<Comment> (commentDto);
            entity.User = await context.Users.FirstOrDefaultAsync (o => o.Id == commentDto.UserId);
            entity.Notification = await context.Notifications.FirstOrDefaultAsync (o => o.Id == commentDto.NotificationId);
            entity.SaveTime = DateTime.Now;
            await context.Comments.AddAsync (entity);
            await context.SaveChangesAsync ();
        }

        public async Task Delete (int id) {
            var comment = await context.Comments.AsNoTracking ().FirstOrDefaultAsync (o => o.Id == id);
            context.Comments.Remove (comment);
            await context.SaveChangesAsync ();
        }
    }
}