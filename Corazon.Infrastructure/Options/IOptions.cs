namespace Corazon.Infrastructure.Options
{
    public interface IOptions
    {
        string SectionKey { get; }
    }
}