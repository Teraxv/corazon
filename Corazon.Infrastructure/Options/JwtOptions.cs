using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Corazon.Infrastructure.Options
{
    public class JwtOptions : IOptions
    {
        public string SectionKey => "Jwt";

        public string SecretKey { get; set; }
        public string Issuer { get; set; }
        public int ExpiryMinutes { get; set; }
        public bool ValidateLifetime { get; set; }
        public bool ValidateAudience { get; set; }   
        public string ValidAudience { get; set; }
        public string AccessClaimName { get; set; }
        
        public SymmetricSecurityKey GetSecurityKey() => new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey));

        public TokenValidationParameters GetTokenValidationParameters()
            => new TokenValidationParameters
            {
                IssuerSigningKey = GetSecurityKey(),
                ValidIssuer = Issuer,
                ValidateLifetime = ValidateLifetime,
                ValidateAudience = ValidateAudience,
                ValidAudience = ValidAudience
            };
    }
}