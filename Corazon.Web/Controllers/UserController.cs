using System.Threading.Tasks;
using Corazon.Core.Dto.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Corazon.Infrastructure.Services.Interfaces;

namespace Corazon.Web.Controllers
{
    [Route ("api/[controller]")]
    public class UserController : Controller {
        private readonly IUserService userService;

        public UserController (IUserService userService) {
            this.userService = userService;
        }

        [HttpPost ("authenticate")]
        public async Task<IActionResult> Authenticate ([FromBody] AuthenticateUserDto model)
            => Json(await userService.AuthenticateAsync(model));

        [HttpPost ("register")]
        public async Task<IActionResult> Register ([FromBody] RegisterUserDto model) {
            await userService.Register(model);
            return Created(string.Empty, null);
        }

        //[Authorize]
        [HttpGet]
        public IActionResult GetUsers () {
            return Json( userService.Browse());
        }
        
        //[Authorize]
        [HttpGet ("{id}")]
        public async Task<IActionResult> GetUserById (int id)
            => Json(await userService.GetAsync(id));

        [Authorize]
        [HttpPut ("update/{id}")]
        public async Task<IActionResult> UpdateUser (int id, [FromBody] UpdateUserDto model) {
            await userService.UpdateAsync(model);
            return Accepted();
        }

        [Authorize]
        [HttpPut ("updatePassword/{id}")]
        public async Task<IActionResult> UpdateUserPassword (int id, string password) {
            await userService.UpdatePasswordAsync(id, password);
            return Accepted();
        }

        [Authorize]
        [HttpDelete ("delete/{id}")]
        public async Task<IActionResult> DeleteUser (int id) {
            await userService.DeleteAsync(id);
            return Accepted();
        }

        [Authorize]
        [HttpGet ("restore/{id}")]
        public async Task<IActionResult> RestoreUser (int id) {
            await userService.RestoreAsync(id);
            return Accepted();
        }

    }

}