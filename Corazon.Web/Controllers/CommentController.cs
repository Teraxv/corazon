using System.Threading.Tasks;
using Corazon.Core.Dto.Comments;
using Corazon.Infrastructure.Hubs;
using Corazon.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace Corazon.Web.Controllers {
    [ApiController]
    [Route ("api/[controller]")]
    public class CommentController : Controller {
        private const int COMMENTS_PER_PAGE = 10;

        private readonly ICommentService commentService;

        public CommentController(ICommentService commentService)
        {
            this.commentService = commentService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCommentsByNotificationAndPage (int page, int notificationId) {
            return Json(await commentService.Get(notificationId, page * COMMENTS_PER_PAGE, COMMENTS_PER_PAGE));
        }

        [HttpPost]
        public async Task<IActionResult> AddNewComment ([FromBody] NewCommentDto commentDto) {
            await commentService.Create(commentDto);            
            return Created(string.Empty, null);
        }

        [HttpDelete ("{id}")]
        public async Task<IActionResult> DeleteComment ([FromRoute] int id) {
            await commentService.Delete(id);
            return Accepted();
        }
    }
}