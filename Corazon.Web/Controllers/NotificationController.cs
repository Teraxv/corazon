using System.Threading.Tasks;
using Corazon.Core.Dto.Notifications;
using Corazon.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Corazon.Web.Controllers {
    [Route ("api/[controller]")]
    public class NotificationController : Controller {
        private readonly INotificationService notificationService;

        public NotificationController(INotificationService notificationService)
        {
            this.notificationService = notificationService;
        }

        //[Authorize]
        [HttpGet]
        public IActionResult GetNotifications ()
            => Json(notificationService.Browse());

        //[Authorize]
        [HttpGet ("assigned/{id}")]
        public IActionResult GetNotificationsByAssignedUserId (int id)
            => Json(notificationService.BrowseByAssignedUser(id));

        //[Authorize]
        [HttpGet ("published/{id}")]
        public IActionResult GetNotificationsByPublishedUserId (int id)
            => Json(notificationService.BrowseByDeclarantUser(id));

        //[Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateNotification ([FromBody] NewNotificationDto notification) {
            await notificationService.Create(notification);
            return Created(string.Empty, null);
        }

        //[Authorize]
        [HttpPut ("{id}")]
        public async Task<IActionResult> UpdateNotification (int id, [FromBody] UpdateNotificationDto notification) {
            notification.Id = id;
            await notificationService.UpdateAsync(notification);
            return Accepted();
        }

        //[Authorize]
        [HttpDelete ("{id}")]
        public async Task<IActionResult> DeleteNotification (int id) {
            await notificationService.DeleteAsync(id);
            return Accepted();
        }
    }
}