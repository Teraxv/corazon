using AutoMapper;
using Corazon.Core.Dto.Comments;
using Corazon.Core.Dto.Notifications;
using Corazon.Core.Dto.Users;
using Corazon.Core.Entities;
using Corazon.Core.Entities.Contexts;
using Corazon.Infrastructure.Hubs;
using Corazon.Infrastructure.Options;
using Corazon.Infrastructure.Services;
using Corazon.Infrastructure.Services.Interfaces;
using Corazon.Web.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Corazon.Web {
    public class Startup {
        private const string SWAGGER_NAME = "Corazon API";

        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices (IServiceCollection services) {

            services.AddCors (o => o.AddPolicy ("CorsPolicy", builder => {
                builder
                    .AllowAnyHeader ()
                    .AllowAnyMethod ()
                    .AllowCredentials ()
                    .WithOrigins (
                        "http://localhost:4200",
                        "http://185.221.83.150:4200");
            }));

            services.AddControllers ();
            services.AddSignalR ();
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", new OpenApiInfo { Title = SWAGGER_NAME, Version = "v1" });
            });
            services.AddMvc ()
                .AddNewtonsoftJson (o => {
                    o.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    o.SerializerSettings.Converters.Add (new StringEnumConverter ());
                    o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });

            services.AddDbContext<EntityContext> (o => o.UseNpgsql (Configuration["ConnectionString"]), ServiceLifetime.Scoped, ServiceLifetime.Singleton);
            services.AddOptions<JwtOptions> (Configuration);
            services.AddSingleton<IMapper> (_ => new MapperConfiguration (o => ConfigureMapper (o)).CreateMapper ());
            services.AddScoped<IUserService, UserService> ();
            services.AddScoped<INotificationService, NotificationService> ();
            services.AddScoped<IAuthorizationService, AuthorizationService> ();
            services.AddScoped<ICommentService, CommentService> ();

            var jwtOptions = Configuration.GetOptions<JwtOptions> ();
            services.AddAuthentication (JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer (config => {
                    config.TokenValidationParameters = jwtOptions.GetTokenValidationParameters ();
                });
        }

        public void Configure (IApplicationBuilder app, IWebHostEnvironment env, EntityContext context) {
            context.Database.EnsureCreated ();

            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
                app.UseSwagger ();
                app.UseSwaggerUI (c => {
                    c.SwaggerEndpoint ("/swagger/v1/swagger.json", SWAGGER_NAME);
                });
            }

            app.UseCors ("CorsPolicy");
            app.UseRouting ();
            app.UseAuthentication ();
            app.UseAuthorization ();

            app.UseEndpoints (endpoints => {
                endpoints.MapControllers ();
                endpoints.MapHub<NotificationHub> ("/noti");
            });
        }

        private void ConfigureMapper (IMapperConfigurationExpression cfg) {
            cfg.CreateMap<NewNotificationDto, Notification> ();
            cfg.CreateMap<RegisterUserDto, User> ();
            cfg.CreateMap<UpdateUserDto, User> ();
            cfg.CreateMap<NewCommentDto, Comment> ();
            cfg.CreateMap<Comment,CommentDto>()
            .ForPath(dest => dest.UserName, opt => opt.MapFrom(src => src.User.Name));
        }
    }
}