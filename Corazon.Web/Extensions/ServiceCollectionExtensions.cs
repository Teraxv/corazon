using Corazon.Infrastructure.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace Corazon.Web.Extensions
{    
    public static class ServiceCollectionExtensions
    {
        public static void AddOptions<TOptions>(this IServiceCollection services, IConfiguration configuration) where TOptions : class, IOptions, new()
        {
            var options = configuration.GetOptions<TOptions>();
            
            services.Configure<TOptions>(configuration.GetSection(options.SectionKey));
            services.AddSingleton(options);
        }
    }
}